from flask import Flask
from flask_caching import Cache

import config

cache = Cache(config={'CACHE_TYPE': 'simple'})

server = Flask(__name__)
server.debug = config.DEBUG
cache.init_app(server)

if __name__ == '__main__':
    manager.run()
