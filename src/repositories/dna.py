""" Defines the Dna repository """

from models import Dna
from server import cache

class DnaRepository:
    """ The repository for the dna model """

    @staticmethod
    def get():
        return Dna.getDna()

    @staticmethod
    def create(length):
        """ Create a new dna """
        dna = Dna(length=length)
        return dna.sequence
