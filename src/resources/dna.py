"""
Define the REST verbs relative to the dna
"""

from flasgger import swag_from
from flask.ext.restful import Resource
from flask.ext.restful.reqparse import Argument
from flask.json import jsonify

from repositories import DnaRepository
from util import parse_params
from server import cache

class DnaResource(Resource):
    """ Verbs relative to the dna """

    @staticmethod
    @swag_from('../swagger/dna/GET.yml')
    def get():
        """ Return a dna sequece information based on his lenght """
        dna = DnaRepository.get()
        return jsonify({'dna': dna})

    @staticmethod
    @swag_from('../swagger/dna/POST.yml')
    def post(length):
        """ Create an dna based on the sent information """
        dna = DnaRepository.create(length=length)
        return jsonify({'dna': dna})
