"""
Defines the blueprint for the users
"""
from flask import Blueprint
from flask.ext.restful import Api

from resources import DnaResource
from server import cache


DNA_BLUEPRINT = Blueprint('dna', __name__)
Api(DNA_BLUEPRINT).add_resource(
    DnaResource,
    '/dna/<int:length>',
    '/dna'
)

